#!/bin/zsh
print 'Preparing environment for NanoAOD-Diff.py'
ulimit -s 12288
if [[ ${1} = "99" ]];
then
    print sourcing/cvmfs/sft.cern.ch/lcg/views/LCG_99/x86_64-centos7-gcc8-opt/setup.sh
    source /cvmfs/cms.cern.ch/cmsset_default.sh
    export SCRAM_ARCH=slc7_amd64_gcc800
    source /cvmfs/sft.cern.ch/lcg/views/LCG_99/x86_64-centos7-gcc8-opt/setup.sh;
    # export LHAPDF_PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_99/MCGenerators/lhapdf/6.3.0/x86_64-centos7-gcc8-opt
    # export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LHAPDF_PATH/lib/
    # export LHAPDF_DATA_PATH=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/:${LHAPDF_PATH}/share/LHAPDF
    export TEST_VARS=IAMWINNAR
    print 'Finished setup'
elif [[ ${1} = "98" ]];
then
    print sourcing /cvmfs/sft.cern.ch/lcg/views/LCG_98python3/x86_64-centos7-gcc8-opt/setup.sh
    source /cvmfs/cms.cern.ch/cmsset_default.sh
    export SCRAM_ARCH=slc7_amd64_gcc800
    source /cvmfs/sft.cern.ch/lcg/views/LCG_98python3/x86_64-centos7-gcc8-opt/setup.sh;
    # export LHAPDF_PATH=/cvmfs/sft.cern.ch/lcg/releases/LCG_98python3/MCGenerators/lhapdf/6.3.0/x86_64-centos7-gcc8-opt
    # export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LHAPDF_PATH/lib/
    # export LHAPDF_DATA_PATH=/cvmfs/sft.cern.ch/lcg/external/lhapdfsets/current/:${LHAPDF_PATH}/share/LHAPDF
    print 'Finished setup'
elif [[ ${1} = "97" ]];
then
    print sourcing /cvmfs/sft.cern.ch/lcg/views/LCG_97/x86_64-centos7-gcc8-opt/setup.sh
    source /cvmfs/cms.cern.ch/cmsset_default.sh
    export SCRAM_ARCH=slc7_amd64_gcc800
    source /cvmfs/sft.cern.ch/lcg/views/LCG_97/x86_64-centos7-gcc8-opt/setup.sh;
    print 'Finished setup'
else;
    print "Failed to choose a setup, aborting"
    # exit [ 21 ]
fi
