# NanoAOD-Diff

Simple script for comparing CMS NanoAOD samples

# Setup
```
#The numerals after setup indicate the LCG stack desired, e.g. LCG98 or LCG99
source setup.sh 98
#Or
source setup.zsh 99
```

# Run examples
```
sh prepare_example.sh
sh run_example.sh
```

#Usage
## Branch report
```
python NanoAOD-Diff.py --input 'list1.txt' --label 'lbl1' --weight 'genWeight/genEventSumw' --input 'list2.txt' --label 'lbl2' --weight 'genWeight/genEventSumw' --stage report
```

## Branch Statistics and automatic plot card
This method will produce a text file with statistics for all branches (except HLT and L1, by default. Optionally add using --doHLT and/or --doL1) and a text file configuring histograms (4-space separated columns of branch, nxbins, xmin, and xmax) for the next stage, named plotCard_<label1>.txt
```
python NanoAOD-Diff.py --input 'list1.txt' --label 'lbl1' --weight 'genWeight/genEventSumw' --input 'list2.txt' --label 'lbl2' --weight 'genWeight/genEventSumw' --stage stats
```

## Branch histograms and plots
This method will use the plotcard(s) to produce histograms and optionally a pdf output. The first sample passed is 'assumed' to be the nominal/denominator, the second is compared against it.
```
python NanoAOD-Diff.py --input 'list1.txt' --label 'lbl1' --weight 'genWeight/genEventSumw' --input 'list2.txt' --label 'lbl2' --weight 'genWeight/genEventSumw' --stage plots --plotcards plotCard_v5.txt  --nThreads 8 --histOut comparison.root --pdfOut comparison.pdf
```

### Additional options
```
# Change number of threads used in implicit multithreading
--nThreads 12

# Compute a per-event weight with a formula. Special kewards are 'genEventSumw,' 'genEventSumw2,' and 'genEventCount' and this meta information is gathered
# from the Runs tree in NanoAOD. Any branch in the NanoAOD Events tree may also be used, or constants, i.e. a cross-section or lumi values
--weight 'genWeight * 87.76 * 1000 * PSweight[2]/genEventSumw'

# Disable multithreading
--noIMT

# #Disable batch processing and draw histograms while running
--noBatch

# Load a formatted plotcard(s) for histograms
--plotcards myplotcard.txt

# Choose the maximum/minimum on the ratio plot, defaulting to 0.95 and 1.05
--rMin 1.3
--rMax 0.7
```