#!/bin/bash
echo 'Preparing environment for NanoAOD-Diff.py'
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc800
echo /cvmfs/sft.cern.ch/lcg/views/LCG_97/x86_64-centos7-gcc8-opt/setup.sh
source /cvmfs/sft.cern.ch/lcg/views/LCG_97/x86_64-centos7-gcc8-opt/setup.sh
ls -ltR
echo "Command line argument is:"
echo python2 NanoAOD-Diff.py "$@"
python2 NanoAOD-Diff.py "$@"
# if [ "$1" != "" ]
# then
#     echo /cvmfs/sft.cern.ch/lcg/views/dev3/${1}/x86_64-centos7-gcc8-opt/setup.sh
#     source /cvmfs/sft.cern.ch/lcg/views/dev3/${1}/x86_64-centos7-gcc8-opt/setup.sh
# else
#     echo /cvmfs/sft.cern.ch/lcg/views/LCG_97/x86_64-centos7-gcc8-opt/setup.sh
#     source /cvmfs/sft.cern.ch/lcg/views/LCG_97/x86_64-centos7-gcc8-opt/setup.sh
# fi
