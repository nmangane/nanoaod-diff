#!/bin/bash
echo 'Running report stage'
python NanoAOD-Diff.py --input 'testV5.root' --label 'v5' --weight 'genWeight/genEventSumw' --input 'testV6.root' --label 'v6' --weight 'genWeight/genEventSumw' --backend RDF --stage report

echo 'Running stats stage and producing plotcard'
python NanoAOD-Diff.py --input 'testV5.root' --label 'v5' --weight 'genWeight/genEventSumw' --input 'testV6.root' --label 'v6' --weight 'genWeight/genEventSumw' --backend RDF --stage stats --nThreads 8

echo 'Running plots stage to produce histograms and pdf using plotcard'
python NanoAOD-Diff.py --input 'testV5.root' --label 'v5' --weight 'genWeight/genEventSumw' --input 'testV6.root' --label 'v6' --weight 'genWeight/genEventSumw' --backend RDF --stage plots --plotcards plotCard_v5.txt  --nThreads 8 --histOut testOut.root --pdfOut testOut.pdf
